Name:           kubectx
Version:        0.7.1
Release:        1%{?dist}
Summary:        Faster way to switch between clusters and namespaces in kubectl

License:        ASLv2
URL:            https://kubectx.dev/
Source0:        https://github.com/ahmetb/%{name}/archive/v%{version}.tar.gz

Requires:       %{name}-ns = %{version}
Requires:       %{name}-ctx = %{version}

%define debug_package %{nil}

%description
meta package to pull in ns and ctx subpackages

%prep
%autosetup

%install
install -d %{buildroot}/%{_bindir} %{buildroot}/%{_datadir}/%{name}
install -p -m 755 ./kubectx %{buildroot}/%{_bindir}
install -p -m 755 ./kubens %{buildroot}/%{_bindir}
install -p -m 644 ./completion/kubectx.bash %{buildroot}/%{_datadir}/%{name}/
install -p -m 644 ./completion/kubectx.fish %{buildroot}/%{_datadir}/%{name}/
install -p -m 644 ./completion/kubectx.zsh %{buildroot}/%{_datadir}/%{name}/
install -p -m 644 ./completion/kubens.bash %{buildroot}/%{_datadir}/%{name}/
install -p -m 644 ./completion/kubens.fish %{buildroot}/%{_datadir}/%{name}/
install -p -m 644 ./completion/kubens.zsh %{buildroot}/%{_datadir}/%{name}/

%package ctx
Summary:        Faster way to switch between clusters and namespaces in kubectl
%description ctx
kubectx is a utility to manage and switch between kubectl contexts.

%package ns
Summary:        Faster way to switch between clusters and namespaces in kubectl
%description ns
kubens is a utility to switch between Kubernetes name spaces.

%files ctx
%license LICENSE
%{_bindir}/kubectx
%{_datadir}/%{name}/kubectx.bash
%{_datadir}/%{name}/kubectx.fish
%{_datadir}/%{name}/kubectx.zsh

%files ns
%license LICENSE
%{_bindir}/kubens
%{_datadir}/%{name}/kubens.bash
%{_datadir}/%{name}/kubens.fish
%{_datadir}/%{name}/kubens.zsh

%files

%changelog
* Sun Feb  9 2020 Max Audron
- 
